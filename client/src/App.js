import React, { Component } from "react";
// import SimpleStorageContract from "./contracts/SimpleStorage.json";
import TribuToken from "./contracts/TribuToken.json"
import getWeb3 from "./getWeb3";

import "./App.css";

class App extends Component {
  state = {
    web3: null,
    accounts: null,
    contract: null,
    name: "",
    symbol: "",
    decimals: "",
    getBalanceAddress: "",

    newValue: "",
    storageValue: ""
  };

  componentDidMount = async () => {
    try {
      this.handleGetBalanceChange = this.handleGetBalanceChange.bind(this);
      this.handleGetBalanceSubmit = this.handleGetBalanceSubmit.bind(this);

      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = TribuToken.networks[networkId];
      const instance = new web3.eth.Contract(
        TribuToken.abi,
        deployedNetwork && deployedNetwork.address,
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState({ web3, accounts, contract: instance }, this.runExample);
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };

  handleGetBalanceChange(event) {
    this.setState({getBalanceAddress: event.target.getBalanceAddress})
    console.log(this.state.getBalanceAddress)
  }

  async handleGetBalanceSubmit(event) {
    event.preventDefault();

    console.log(this.state.getBalanceAddress)

    const {accounts, contract} = this.state;
    const response = await contract.methods.balanceOf(this.state.getBalanceAddress).send({from: accounts[0]});
    // const response = await contract.methods.get().call();
    this.setState({ getBalanceAddress: response });
  }

  runExample = async () => {
    const { contract } = this.state;

    const name = await contract.methods.name().call();
    const symbol = await contract.methods.symbol().call()
    const decimals = await contract.methods.decimals().call()

    // Update state with the result.
    this.setState({ name: name, symbol: symbol, decimals: decimals });
  };

  render() {
    if (!this.state.web3) {
      return <div>Loading Web3, accounts, and contract...</div>;
    }
    return (
      <div className="App">
        <h2> TribuToken </h2>
        <div> Token Name: {this.state.name} </div>
        <div> Token Symbol: {this.state.symbol} </div>
        <div> Token Decimals: {this.state.decimals} </div>

        <div> Get Balance of:
          <form onSubmit={this.handleGetBalanceSubmit}>
            <input type="text" value={this.state.getBalanceAddress} onChange={this.handleGetBalanceChange.bind(this)}/>
            <input type="submit" value="Submit"/>
          </form>
        </div>
      </div>
    );
  }
}

export default App;
