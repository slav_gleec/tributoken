// SPDX-License-Identifier: MIT

pragma solidity >=0.4.21 <0.7.0;

import "openzeppelin-solidity/contracts/token/ERC20/ERC20Capped.sol";

contract TribuToken is ERC20Capped {

    constructor (string memory name, string memory symbol, uint8 decimals, uint256 cap)
        ERC20(name, symbol) ERC20Capped(cap) public {
        _setupDecimals(decimals);
        _mint(_msgSender(), cap);
    }

    function _beforeTokenTransfer(address from, address to, uint256 amount) internal override {
        super._beforeTokenTransfer(from, to, amount);
    }
}