const Web3 = require("web3")

// var SimpleStorage = artifacts.require("./SimpleStorage.sol");
var TribuToken = artifacts.require("./TribuToken.sol");

const TribuTokenConfig = {
  name: "TRIBU",
  symbol: "TRB",
  decimal: 6,
  cap: Web3.utils.toBN('100000000000000000000000000'),
}

let { name, symbol, decimal, cap } = TribuTokenConfig

module.exports = function(deployer) {
  deployer.deploy(TribuToken, name, symbol, decimal, cap);
};
